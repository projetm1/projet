﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Serveur.modele
{
    public class BC:BaseModele
    {
        private int idUser;
        private int idClientFournisseur;
        private string date;
        private string remarque;
        private bool isClient;

        public bool IsClient
        {
            get { return isClient; }
            set { isClient = value; }
        }
        public int IdUser
        {
            get { return idUser; }
            set { idUser = value; }
        }

        public int IdClientFournisseur
        {
            get { return idClientFournisseur; }
            set { idClientFournisseur = value; }
        }

        public string Date
        {
            get { return date; }
            set { date = value; }
        }

        public string Remarque
        {
            get { return remarque; }
            set { remarque = value; }
        }

        public BC() { isClient = true; }
        public BC(int id) : base(id) { isClient = true; }
    }
}