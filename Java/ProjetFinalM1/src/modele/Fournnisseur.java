/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

/**
 *
 * @author Tsito
 */
public class Fournnisseur {
    private String nom;
    private String contact;
    private String email;

    public Fournnisseur() {
    }

    public Fournnisseur(String nom, String contact, String email) {
        this.nom = nom;
        this.contact = contact;
        this.email = email;
    }

    
    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
}
