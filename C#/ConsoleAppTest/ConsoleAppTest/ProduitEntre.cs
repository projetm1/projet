﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest.modele
{
    class ProduitEntre:BaseModele
    {
        private string designation;

        public string Designation
        {
            get { return designation; }
            set { designation = value; }
        }

        public ProduitEntre(int id) : base(id) { }
        public ProduitEntre(string designation)
        {
            this.designation = designation;
        }

        public ProduitEntre() { }

        public override string ToString()
        {
            return Designation;
        }
    }
}
