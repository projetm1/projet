﻿using ConsoleAppTest.modele;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest.dao
{
    class UtilisateurDAO
    {
        public static void save(Utilisateur u)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                save(u, conn);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        public static void save(Utilisateur u, SqlConnection conn)
        {
            string query = "INSERT INTO _Utilisateur (nom,prenom,email,mdp) VALUES (@nom,@prenom,@email,@mdp)";
            try
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@nom", u.Nom);
                cmd.Parameters.AddWithValue("@prenom", u.Prenom);
                cmd.Parameters.AddWithValue("@email", u.Email);
                cmd.Parameters.AddWithValue("@mdp", u.Mdp);
                cmd.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                if (e.Message.Contains(" UNIQUE KEY constraint"))
                    throw new Exception("Un compte est déjà lié à cet adresse email, veuillez choisir un autre");
                throw e;
            }
            catch (Exception e)
            {
                throw new Exception("impossible de sauvegarder :" + e.Message);
            }
        }
        public static void update(Utilisateur u)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                update(u, conn);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        public static void update(Utilisateur u, SqlConnection conn)
        {
            string query = "UPDATE _UTILISATEUR SET nom=@nom, prenom=@prenom, email=@email, mdp=@mdp WHERE id=@id";
            try
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@id", u.Id);
                cmd.Parameters.AddWithValue("@nom", u.Nom);
                cmd.Parameters.AddWithValue("@prenom", u.Prenom);
                cmd.Parameters.AddWithValue("@email", u.Email);
                cmd.Parameters.AddWithValue("@mdp", u.Mdp);
                cmd.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                if (e.Message.Contains(" UNIQUE KEY constraint"))
                    throw new Exception("Un compte est déjà lié à cet adresse email, veuillez choisir un autre");
                throw e;
            }
            catch (Exception e)
            {
                Console.WriteLine("L'erreur suivante a été rencontrée :" + e.Message);
            }
        }
        public static void findById(Utilisateur u)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                findById(u, conn);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        public static void findById(Utilisateur u, SqlConnection conn)
        {
            string query = "select TOP(1) * from _UTILISATEUR where id = @id";
            SqlDataReader dataReader = null;
            try
            {
                if (u.Id == 0)
                    throw new Exception("Veuillez ajouter un ID");
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@id", u.Id);
                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    u.Nom = (string)dataReader["nom"];
                    u.Prenom = (string)dataReader["prenom"];
                    u.Email = (string)dataReader["email"];
                    u.Mdp = (string)dataReader["mdp"];
                }

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }
        public static void authentifier(Utilisateur u)
        {
            SqlConnection conn = null;
            SqlDataReader dataReader = null;
            try
            {
                conn = DbConnect.getConnexion();
                string query = "select top(1) * from _UTILISATEUR where email=@email and mdp=@mdp";
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@email", u.Email);
                cmd.Parameters.AddWithValue("@mdp", u.Mdp);
                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    u.Id = (int)dataReader["id"];
                    u.Nom = (string)dataReader["nom"];
                    u.Prenom = (string)dataReader["prenom"];
                }
            }
            catch (Exception e)
            {
                throw new Exception("Impossible de vous connecter: " + e.Message);
            }
            finally
            {
                if (conn != null)
                    conn.Close();
                if (dataReader != null)
                    dataReader.Close();
            }
        }

        public static void delete(Utilisateur u)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                delete(u, conn);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        public static void delete(Utilisateur u, SqlConnection conn)
        {
            string query = "DELETE FROM _UTILISATEUR WHERE id=@id";
            try
            {
                if (u.Id == 0)
                    throw new Exception("Suppression impossible, ID manquant");
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@id", u.Id);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
