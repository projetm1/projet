﻿using Serveur.modele;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Serveur.dao
{
    public class UtilisateurDao
    {
        public static void save(Utilisateur u)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                save(u, conn);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        public static void save(Utilisateur u, SqlConnection conn)
        {
            string query = "INSERT INTO Utilisateur (nom,prenom,email,mdp) VALUES ('" + u.Nom + "','" + u.Prenom + "','" + u.Email + "','" + u.Mdp + "')";
            try
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("impossible de sauvegarder :"+e.Message);
            }
        }
        public static void update(Utilisateur u)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                update(u, conn);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        public static void update(Utilisateur u, SqlConnection conn)
        {
            string query = "UPDATE Utilisateur SET (nom='" + u.Nom + "', prenom='" + u.Prenom + "', email='" + u.Email + "', mdp='" + u.Mdp + "' WHERE id=" + u.Id + ")";
            try
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Console.WriteLine("L'erreur suivante a été rencontrée :" + e.Message);
            }
        }
        public static Utilisateur findById(Utilisateur u)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                return findById(u, conn);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        public static Utilisateur findById(Utilisateur u, SqlConnection conn)
        {
            string query = "SELECT * FROM Utilisateur where id = " + u.Id + "";
            Utilisateur rep = new Utilisateur();
            SqlDataReader dataReader = null;
            try
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    rep.Id = (int)dataReader["id"];
                    rep.Nom = (string)dataReader["nom"];
                    rep.Prenom = (string)dataReader["prenom"];
                    rep.Email = (string)dataReader["email"];
                    rep.Mdp = (string)dataReader["mdp"];
                }
                return rep;

            }
            catch (Exception e)
            {
                throw e;
            }finally{
                if (dataReader != null)
                    dataReader.Close();
            }
        }
        public static Utilisateur authentifier(string email, string mdp)
        {
            SqlConnection conn = null;
            SqlDataReader dataReader = null;
            try
            {
                conn = DbConnect.getConnexion();
                string query = "select * from utilisateur where email='" + email + "' and mdp='" + mdp + "'";
                Utilisateur rep = new Utilisateur(0);
                SqlCommand cmd = new SqlCommand(query, conn);
                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    rep.Id = (int)dataReader.GetValue(0);
                    rep.Nom = (string)dataReader.GetValue(1);
                    rep.Prenom = (string)dataReader.GetValue(2);
                    rep.Email = (string)dataReader.GetValue(3);
                    rep.Mdp = (string)dataReader.GetValue(4);
                }
                return rep;
            }
            catch (Exception e)
            {
                throw new Exception("Impossible de vous connecter: "+e.Message);
            }
            finally
            {
                if (conn != null)
                    conn.Close();
                if (dataReader!= null)
                    dataReader.Close();
            }
        }

        /*public static List<Utilisateur> findAll(SqlConnection conn){
            string query = "SELECT * FROM Utilisateur";
            List<Utilisateur> list = 
            try{
                MySqlCommand cmd = new MySqlCommand(query, conn);
                MySqlDataReader dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    list[0].Add(dataReader["id"] + "");
                    list[1].Add(dataReader["name"] + "");
                    list[2].Add(dataReader["age"] + "");
                }
            }
            catch(Exception e){
                Console.WriteLine("L'erreur suivante a été rencontrée :" + e.Message);
            }
        }*/
    }
}