﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest
{
    class FactureDetail : BaseModele
    {
        private int factureID;
        private int produitID;
        private string designation;
        private double pu;
        private int quantite;
        private string remarque;

        public string Remarque
        {
            get { return remarque; }
            set { remarque = value; }
        }

        public int Quantite
        {
            get { return quantite; }
            set { quantite = value; }
        }

        public double Pu
        {
            get { return pu; }
            set { pu = value; }
        }

        public string Designation
        {
            get { return designation; }
            set { designation = value; }
        }

        public int ProduitID
        {
            get { return produitID; }
            set { produitID = value; }
        }

        public int FactureID
        {
            get { return factureID; }
            set { factureID = value; }
        }

        public FactureDetail() { Remarque = ""; }
        public FactureDetail(int id)
            : base(id)
        {
            Remarque = "";
        }
        public FactureDetail(int id, int factureID, int produitID, string designation, double pu, int quantite, string remarque)
        {
            this.Id = id;
            this.FactureID = factureID;
            this.ProduitID = produitID;
            this.Designation = designation;
            this.Pu = pu;
            this.Quantite = quantite;
            this.Remarque = remarque;
        }
        public override string ToString()
        {
            return "Fact n" + factureID + " | p" + produitID + " " + Designation + " | pu:" + Pu + " | QTT:" + Quantite;
        }
    }
}
