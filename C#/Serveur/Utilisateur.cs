﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Serveur.modele
{
    public class Utilisateur:BaseModele
    {
        private string nom;
        private string prenom;
        private string email;
        private string mdp;

        public string Mdp
        {
            get { return mdp; }
            set { mdp = value; }
        }

        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }

        public string Prenom
        {
            get { return prenom; }
            set { prenom = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public Utilisateur() { }

        public Utilisateur(int id) : base(id) { }
    }
}