/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tsito
 */
public class BonDeCommandeModele extends BaseModele {

    private Fournnisseur f;
    private String date;
    private List<Produit> listProduit = new ArrayList<Produit>();
    private double prixTotal;

    public BonDeCommandeModele() {
    }

    public BonDeCommandeModele(Fournnisseur f) {
        this.f = f;
    }

    public BonDeCommandeModele(Fournnisseur f, String date, double prixTotal) {
        this.f = f;
        this.date = date;
        this.prixTotal = prixTotal;
    }
    
    
    public Fournnisseur getF() {
        return f;
    }

    public void setF(Fournnisseur f) {
        this.f = f;
    }

    
    

    public List<Produit> getListProduit() {
        return listProduit;
    }

    public void setListProduit(List<Produit> listProduit) {
        this.listProduit = listProduit;
    }

    public double getPrixTotal() {
        return prixTotal;
    }

    public void setPrixTotal(double prixTotal) {
        this.prixTotal = prixTotal;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    
}
