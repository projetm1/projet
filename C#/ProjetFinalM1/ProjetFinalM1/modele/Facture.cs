﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest.modele
{
    class Facture : BaseModele
    {
        private Fournisseur fournisseur;
        private int utilisateurID;
        private string daty;
        private int tva;
        private double montant;
        private string remarque;
        private double avoir;
        private List<FactureDetail> details;
        private string reference;
        private int vise;

        public int Vise
        {
            get { return vise; }
            set { vise = value; }
        }

        public string Reference
        {
            get { return reference; }
            set { reference = value; }
        }


        internal List<FactureDetail> Details
        {
            get { return details; }
            set { details = value; }
        }

        public double Avoir
        {
            get { return avoir; }
            set { avoir = value; }
        }

        public string Remarque
        {
            get { return remarque; }
            set { remarque = value; }
        }

        public double Montant
        {
            get { return montant; }
            set { montant = value; }
        }

        public int Tva
        {
            get { return tva; }
            set { tva = value; }
        }

        public string Daty
        {
            get { return daty; }
            set { daty = value; }
        }

        public int UtilisateurID
        {
            get { return utilisateurID; }
            set { utilisateurID = value; }
        }

        internal Fournisseur Fournisseur
        {
            get { return fournisseur; }
            set { fournisseur = value; }
        }

        public void addDetails(FactureDetail fd)
        {
            details.Add(fd);
        }

        public Facture()
        {
            this.Details = new List<FactureDetail>();
            this.Fournisseur = new Fournisseur();
            this.Remarque = "";
        }

        public Facture(int id)
            : base(id)
        {
            this.Details = new List<FactureDetail>();
            this.Fournisseur = new Fournisseur();
            this.Remarque = "";
        }

        public override string ToString()
        {
            string r = Fournisseur.Id + ":" + Fournisseur.Nom + " | " + Fournisseur.Email + " | " + Fournisseur.Tel + " | " + Fournisseur.Adresse + "\n\r";
            for (int i = 0; i < Details.Count; i++)
            {
                r += "-" + Details[i].Designation + " | " + Details[i].Pu + " | " + Details[i].Quantite + "\n\r";
            }
            return r;
        }
    }
}
