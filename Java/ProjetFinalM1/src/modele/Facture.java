/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import java.util.ArrayList;

/**
 *
 * @author Rasolonirina
 */
public class Facture extends BaseModele {

    String date;
    String remarque;
    String fournisseur;
    String total;
    String reference;
    String TVA;
    ArrayList<DetailFacture> arrayList;

    public Facture(String date, String remarque, String fournisseur, String total, String reference, String TVA, ArrayList<DetailFacture> arrayList) {
        this.date = date;
        this.remarque = remarque;
        this.fournisseur = fournisseur;
        this.total = total;
        this.reference = reference;
        this.TVA = TVA;
        this.arrayList = arrayList;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

    public String getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(String fournisseur) {
        this.fournisseur = fournisseur;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getTVA() {
        return TVA;
    }

    public void setTVA(String TVA) {
        this.TVA = TVA;
    }

    public ArrayList<DetailFacture> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<DetailFacture> arrayList) {
        this.arrayList = arrayList;
    }

    public void affiche() {
        System.out.println("Facture{" + "date=" + date + ", remarque=" + remarque + ", fournisseur=" + fournisseur + ", total=" + total + ", reference=" + reference + ", TVA=" + TVA + '}');
        for (DetailFacture row : arrayList) {
            row.affiche();
        }
    }

    public Facture() {
    }

}
