/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

import java.util.ArrayList;
import java.util.List;
import modele.BonDeCommandeModele;
import modele.CV;
import modele.Fournnisseur;
import modele.Produit;

/**
 *
 * @author Rasolonirina
 */
public class Numerisation {
    public static CV toCV(String cvStr, boolean isUsuel) {
        CV cv = new CV();
        if (isUsuel) {
            String lines[] = cvStr.split("\\r?\\n");
            cv.setNom(lines[1]);
            cv.setAdresse(lines[2]);
            cv.setTel(lines[3]);
            cv.setEmail(lines[4]);
            cv.setPoste(lines[5]);
            cv.setType(lines[6]);
        }
        return cv;
    }
    public static BonDeCommandeModele toBDCclient(String bdcStr) throws Exception {
        BonDeCommandeModele rep = new BonDeCommandeModele();
        return rep;
    }
    public static BonDeCommandeModele toBdc(String bdcStr) throws Exception {
        BonDeCommandeModele rep = new BonDeCommandeModele();
        String[] lines = bdcStr.split("\\r?\\n");
        /*for (int i = 0; i < lines.length; i++) {
            System.out.println(i + " = " + lines[i]);
        }*/
        Fournnisseur f = new Fournnisseur();
        String[] s;
        s = lines[1].split(" ");
        f.setNom(s[s.length - 1]);
        s = lines[2].split(" ");
        f.setContact(s[2]);
        s = lines[3].split(" ");
        f.setEmail(s[2]);
        String date = "";
        for(int t=s.length-4;t<s.length;t++){
            date = date+" "+s[t];
        }
        rep.setDate(date);
        rep.setF(f);
        int nbProduit = 0;
        int i = 8;
        while (Numerisation.toInt(lines[i])) {
            nbProduit++;
            i++;
        }
        Produit p = new Produit();
        List<Produit> lisProduit = new ArrayList<Produit>();
        int quantiteLine, designationLine, prixUnitaireLine, prixTotalLine;
        String prixUnitaireValue ="";
        String prixTotalValue="";
        quantiteLine = 8;
        designationLine = quantiteLine + nbProduit;
        prixUnitaireLine = designationLine + nbProduit;
        prixTotalLine = prixUnitaireLine + nbProduit;
        for (int j = 0; j < nbProduit; j++) {
            p = new Produit();
            p.setQuantite(Integer.parseInt(lines[quantiteLine]));
            p.setDesignation(lines[designationLine]);
            s = lines[prixUnitaireLine].split(" ");
            if(s.length!=1){
                for(int u=0;u<s.length;u++){
                  prixUnitaireValue = prixUnitaireValue.concat(s[u]);
                }
            }
            p.setPrixUnitaire(Double.parseDouble(prixUnitaireValue));
            s = lines[prixTotalLine].split(" ");
            if(s.length!=1){
                for(int u=0;u<s.length;u++){                  
                  prixTotalValue = prixTotalValue.concat(s[u]);
                }
            }
            p.setPrixTotal(Double.parseDouble(prixTotalValue));
            lisProduit.add(p);
            prixUnitaireValue="";
            prixTotalValue ="";
            quantiteLine++;
            designationLine++;
            prixUnitaireLine++;
            prixTotalLine++;
        }
        rep.setListProduit(lisProduit);
        s = lines[prixTotalLine].split(" ");
        String total = "";
        int taille = s.length-3;
        for(int u=3;u<3+taille;u++){                  
                  total = total.concat(s[u]);
        }
        rep.setPrixTotal(Double.parseDouble(total));
        return rep;
    }

    public static boolean toInt(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }
}
