﻿using ConsoleAppTest.modele;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace ProjetFinalM1
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom d'interface "ICsService" à la fois dans le code et le fichier de configuration.
    [ServiceContract]
    public interface ICsService
    {

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/authentifier/{email}/{mdp}",
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        Utilisateur Authentifier(string email, string mdp);

        Utilisateur Inscription(Utilisateur u);

        Fournisseur saveFounisseur(string jeton, Fournisseur f);

        List<Fournisseur> saveFournisseurAndGetAll(string jeton, Fournisseur f);
        void saveFacture(string jeton, Facture f);
    }
}
