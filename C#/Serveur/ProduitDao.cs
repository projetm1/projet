﻿using Serveur.modele;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Serveur.dao
{
    public class ProduitDao
    {
        public static void save(Produit p)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                save(p, conn);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        public static void save(Produit p, SqlConnection conn)
        {
            string table = "ProduitSortie";
            if (p.Entre) table = "ProduitEntre";
            string query = "INSERT INTO " + table + " (id,designation,pu) values ("+p.Id+",'"+p.Designation+"',"+p.Pu+")";
            try
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message, null);
            }
        }
        public static void findByNameAndSaveIfNotExist(Produit p, SqlConnection conn)
        {
            findByName(p, conn);
            if (p.Id == 0)
            {
                save(p, conn);
            }
        }

        public static void findByName(Produit p, SqlConnection conn)
        {
            string table = "ProduitSortie";
            if(p.Entre) table = "ProduitEntre";
            string query = "SELECT TOP(1) * FROM " + table + " where designation  = '" + p.Designation + "'";
            SqlDataReader dataReader = null;
            try
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    p.Id = (int)dataReader["id"];
                    p.Pu = (double)dataReader["pu"];
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }
    }
}