/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

/**
 *
 * @author Kiady Asandratra
 */
public class DetailFacture {
    String quantite;
    String produit;
    String pu;
    String total;

    public DetailFacture() {
    }

    public DetailFacture(String quantite, String produit, String pu, String total) {
        this.quantite = quantite;
        this.produit = produit;
        this.pu = pu;
        this.total = total;
    }

 
    public void affiche(){
        System.out.println(this);
    }

    @Override
    public String toString() {
        return "DetailFacture{" + "quantite=" + quantite + ", produit=" + produit + ", pu=" + pu + ", total=" + total + '}';
    }

    public String getQuantite() {
        return quantite;
    }

    public void setQuantite(String quantite) {
        this.quantite = quantite;
    }

    public String getProduit() {
        return produit;
    }

    public void setProduit(String produit) {
        this.produit = produit;
    }

    public String getPu() {
        return pu;
    }

    public void setPu(String pu) {
        this.pu = pu;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

}
