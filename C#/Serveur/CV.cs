﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Serveur.modele
{
    public class CV:BaseModele
    {
        private string nom;
        private string tel;
        private string email;
        private string type;
        private string poste;
        private string adresse;
        private string societe;

        public string Societe
        {
            get { return societe; }
            set { societe = value; }
        }

        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }

        public string Tel
        {
            get { return tel; }
            set { tel = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public string Adresse
        {
            get { return adresse; }
            set { adresse = value; }
        }

        public string Poste
        {
            get { return poste; }
            set { poste = value; }
        }

        public string Type
        {
            get { return type; }
            set { type = value; }
        }

        public CV() { }
        public CV(int id) : base(id) { }
    }
}