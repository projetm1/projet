﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest.modele
{
    class Utilisateur:Personne
    {
        private string mdp;

        public string Mdp
        {
            get { return mdp; }
            set { mdp = value; }
        }
        public Utilisateur() { }
        public Utilisateur(int id) : base(id) { }

        public override string ToString()
        {
            return this.Nom + " " + this.Prenom + " | " + this.Email + " | " + this.Mdp;
        }
    }
}
