﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Serveur.modele
{
    public class FactureDetail:BaseModele
    {
        private int quantite;
        private int pu;
        private int factureId;
        private string designation;
        private int produitId;

        public int ProduitId
        {
            get { return produitId; }
            set { produitId = value; }
        }

        public string Designation
        {
            get { return designation; }
            set { designation = value; }
        }

        public int FactureId1
        {
            get { return factureId; }
            set { factureId = value; }
        }

        public int Quantite
        {
          get { return quantite; }
          set { quantite = value; }
        }


        public int Pu
        {
          get { return pu; }
          set { pu = value; }
        }

        public FactureDetail() { }
        public FactureDetail(int quantite, String designation, int pu, int total)
        {
            this.Quantite = quantite;
            this.Designation = designation;
            this.Pu = pu;
        }

        public string FactureId { get; set; }
    }
}