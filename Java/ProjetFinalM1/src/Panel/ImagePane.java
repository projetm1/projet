/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Panel;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 *
 * @author Rasolonirina
 */
public class ImagePane extends JPanel{
    private static final long serialVersionUID = 1L;
	private Image img;
        public ImagePane(){}
	public ImagePane(String img) {
            this(new ImageIcon(img).getImage());
	}
	public ImagePane(Image img) {
		this.img = img;
		Dimension size = new Dimension(1375, 700);
		setPreferredSize(size);
		setMinimumSize(size);
		setMaximumSize(size);
		setSize(size);
		setLayout(null);
	}
 
	public void paintComponent(Graphics g) {
		g.drawImage(img, 0, 0, null);
	}
}
