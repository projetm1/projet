﻿using Serveur.modele;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Serveur.dao
{
    public class CarteVisiteDao
    {
        public static void save(CarteVisite cv)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                save(cv, conn);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        public static void save(CarteVisite cv, SqlConnection conn){
            string query = "INSERT INTO CARTE_VISITE (nom,societe,profesion) values ";
            try
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message, null);
            }
        }
        
        public static int getLastId(SqlConnection conn)
        {
            string query = "SELECT TOP(1) id FROM CARTE_VISITE ORDER BY 1 DESC";
            SqlDataReader dataReader = null;
            try
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    return (int)dataReader["id"];
                }
                dataReader.Close();
                return 0;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }
        public static void update(CarteVisite cv)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                update(cv, conn);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void update(CarteVisite cv, SqlConnection conn)
        {
            string query = "UPDATE CARTE_VISITE SET (nom='" + cv.Nom + "', societe='" + cv.Societe + "', profession='" + cv.Profession + "')";
            try
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static CarteVisite findById(CarteVisite cv)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                return findById(cv, conn);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        public static CarteVisite findById(CarteVisite cv, SqlConnection conn)
        {
            string query = "SELECT * FROM CARTE_VISITE where id=" + cv.Id;
            CarteVisite rep = new CarteVisite();
            SqlDataReader dataReader = null;
            try
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    rep.Id = (int)dataReader["id"];
                    rep.Nom = (string)dataReader["nom"];
                    rep.Societe = (string)dataReader["societe"];
                    rep.Profession = (string)dataReader["profession"];
                }
                return rep;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }
        public static void delete(CarteVisite cv)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                delete(cv, conn);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        public static void delete(CarteVisite cv, SqlConnection conn)
        {
            string query = "DELETE FROM CARTE_VISITE WHERE id=" + cv.Id + "";
            try
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /*public static List<Facture> findAll(SqlConnection conn){
            ArrayList<Facture> list = new ArrayList<Facture>();
            string strRequete = "SELECT * FROM Facture";
            try{
                SqlCommand oCommand = new SqlCommand(strRequete,oConnection);
                oConnection.Open();
                SqlDataReader oReader = oCommand.ExecuteReader();
                do
                {
                }
            }
            catch(Exception e){
                Console.WriteLine("L'erreur suivante a été rencontrée :" + e.Message);
            }
            return list;
        }*/
    }
}