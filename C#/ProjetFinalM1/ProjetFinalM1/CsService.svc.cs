﻿using ConsoleAppTest.dao;
using ConsoleAppTest.modele;
using ProjetFinalM1.modele;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ProjetFinalM1
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom de classe "CsService" à la fois dans le code, le fichier svc et le fichier de configuration.
    // REMARQUE : pour lancer le client test WCF afin de tester ce service, sélectionnez CsService.svc ou CsService.svc.cs dans l'Explorateur de solutions et démarrez le débogage.
    public class CsService : ICsService
    {
        private Jetons jetons = new Jetons();
        public Utilisateur Authentifier(string email, string mdp)
        {
            try
            {
                Utilisateur u = new Utilisateur();
                u.Email = email;
                u.Mdp = mdp;
                UtilisateurDAO.authentifier(u);
                u.Jeton = JetonsHelper.GenerateJeton(jetons, u.Email, u.Mdp);
                return u;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Utilisateur Inscription(Utilisateur u)
        {
            try
            {
                UtilisateurDAO.save(u);
                u.Jeton = JetonsHelper.GenerateJeton(jetons, u.Email, u.Mdp);
                return u;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Fournisseur saveFounisseur(string jeton, Fournisseur f)
        {
            try
            {
                JetonsHelper.Authentifier(jeton, jetons);
                FournisseurDAO.save(f);
                return f;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Fournisseur> saveFournisseurAndGetAll(string jeton, Fournisseur f)
        {
            SqlConnection conn = null;
            try
            {
                JetonsHelper.Authentifier(jeton, jetons);
                conn = DbConnect.getConnexion();
                FournisseurDAO.save(f);
                return FournisseurDAO.findAll();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (conn.State == System.Data.ConnectionState.Open)
                    conn.Close();
            }
        }

        public void saveFacture(string jeton, Facture f)
        {
            try
            {
                JetonsHelper.Authentifier(jeton, jetons);
                FactureDAO.save(f);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
