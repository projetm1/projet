﻿using ConsoleAppTest.modele;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest.dao
{
    class ProduitDAO
    {
        public static void save(ProduitEntre p)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                save(p, conn);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        public static void save(ProduitEntre p, SqlConnection conn)
        {
            string query = "INSERT INTO ";
            try
            {
                SqlCommand cmd = null;
                if (p is ProduitAvendre)
                {
                    query = query + "_PRODUIT_AVENDRE (designation, pu) output INSERTED.ID values (@designation, @pu)";
                    cmd = new SqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@pu", ((ProduitAvendre)p).Pu);
                }
                else
                {
                    query = query + "_PRODUIT_ENTRE (designation) output INSERTED.ID values (@designation)";
                    cmd = new SqlCommand(query, conn);
                }
                cmd.Parameters.AddWithValue("@designation", p.Designation);
                p.Id = (int)cmd.ExecuteScalar();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message, null);
            }
        }

        public static void update(ProduitEntre p)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                update(p, conn);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        public static void update(ProduitEntre p, SqlConnection conn)
        {
            string query = "";
            try
            {
                if (p.Id == 0)
                    throw new Exception("Mise à jour impossible, ID manquant");
                SqlCommand cmd = null;
                if (p is ProduitAvendre)
                {
                    query = "UPDATE _PRODUIT_AVENDRE SET designation=@designation, pu=@pu where id=@id";
                    cmd = new SqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@id", p.Id);
                    cmd.Parameters.AddWithValue("@pu", ((ProduitAvendre)p).Pu);
                }
                else
                {
                    query = "UPDATE _PRODUIT_ENTRE SET designation=@designation where id=@id";
                    cmd = new SqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@id", p.Id);
                }
                cmd.Parameters.AddWithValue("@designation", p.Designation);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message, null);
            }
        }

        public static void findById(ProduitEntre p)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                findById(p, conn);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (conn.State == System.Data.ConnectionState.Open)
                    conn.Close();
            }
        }
        public static void findById(ProduitEntre p, SqlConnection conn)
        {
            string query = "SELECT top(1) * from ";
            SqlDataReader dataReader = null;
            try
            {
                if (p.Id == 0)
                    throw new Exception("Recherche impossible, ID manquant");
                if (p is ProduitAvendre)
                {
                    query += "_PRODUIT_AVENDRE ";
                }
                else
                {
                    query += "_PRODUIT_ENTRE ";
                }
                query += "where id=@id";
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@id", p.Id);
                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    p.Designation = (string)dataReader["designation"];
                    if (p is ProduitAvendre)
                    {
                        ((ProduitAvendre)p).Pu = Convert.ToDouble(dataReader["pu"]);
                    }
                }

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }

        public static void delete(ProduitEntre p)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                delete(p, conn);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (conn.State == System.Data.ConnectionState.Open)
                    conn.Close();
            }
        }
        public static void delete(ProduitEntre p, SqlConnection conn)
        {
            try
            {
                if (p.Id == 0)
                    throw new Exception("Suppression impossible, ID manquant");
                string query = "DELETE FROM ";
                if (p is ProduitAvendre)
                {
                    query += "_PRODUIT_AVENDRE ";
                }
                else
                {
                    query += "_PRODUIT_ENTRE";
                }
                query += " where id=@id";
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@id", p.Id);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public static void findByNameAndSaveIfNotExist(ProduitEntre p, SqlConnection conn)
        {
            findByName(p, conn);
            if (p.Id == 0)
            {
                save(p, conn);
            }
        }

        public static void findByName(ProduitEntre p, SqlConnection conn)
        {
            string table = "_Produit_Entre";
            if (p is ProduitAvendre)
                table = "_Produit_Avendre";
            string query = "SELECT TOP(1) * FROM " + table + " where designation = @designation";
            SqlDataReader dataReader = null;
            try
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@designation", p.Designation);
                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    p.Id = (int)dataReader["id"];
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }
    }
}
