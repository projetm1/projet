
package metier;

import com.asprise.ocr.Ocr;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;


public class Read {
    public static String imgToString(String filepath)throws Exception{
        String rep ="";
        try{
            Ocr.setUp();
            Ocr ocr = new Ocr();
            ocr.startEngine("eng", Ocr.SPEED_FASTEST);
            rep = ocr.recognize(new File[] {new File(filepath)},
            Ocr.RECOGNIZE_TYPE_ALL, Ocr.OUTPUT_FORMAT_PLAINTEXT);
            ocr.stopEngine();
        }
        catch(Exception e){
            System.out.println(e.getMessage());
            throw e;
        }      
        return rep;
    }
    public static int identificationDocument(String s){
        int rep = -1;
        try{
           String lines[] = s.split("\\r?\\n");
            System.out.println(lines[0]);
            System.out.println(lines[1]);
           if(lines[0].compareToIgnoreCase("Carte de visite")==0){
               rep = 0;
           }
           if(lines[0].compareToIgnoreCase("BoN dE commande")==0){
               String fc[] = lines[1].split(" ");
               if(fc[0].compareToIgnoreCase("Client")==0) rep = 3;
               else rep = 1;
           }
           if(lines[1].compareToIgnoreCase("Facture")==0){
               rep = 2;
           } 
        }
        catch(Exception e){
            System.out.println(e.getMessage());
            throw e;
        }
        return rep;
    }
    public static String read(String file){
        BufferedReader br = null;
        String read = "";
        try{
            String current;
            br = new BufferedReader(new FileReader(file));
            while((current = br.readLine() ) !=null){
                read = read+current+"\n";
            }
        }
        catch(Exception e){
            System.out.println(e.getMessage()); 
        }
        finally{
            try{
               if(br!=null) br.close();
            }
            catch(Exception e){
                System.out.println(e.getMessage());
            }
        }
        return read;
    }
}
