﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Serveur.modele
{
    public class CarteVisite:BaseModele
    {
        private string nom;

        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }
        private string societe;

        public string Societe
        {
            get { return societe; }
            set { societe = value; }
        }
        private string profession;

        public string Profession 
        {
            get { return profession; }
            set { profession = value; }
        }
        public CarteVisite() { }
        public CarteVisite(int id) : base(id) { }
    }
}