﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest.modele
{
    class Fournisseur : Personne
    {
        private string logo;

        public string Logo
        {
            get { return logo; }
            set { logo = value; }
        }
        public Fournisseur() { }

        public Fournisseur(int id): base(id){}

        public Fournisseur(int id, string nom, string email, string tel, string adresse, string logo):base(id) {
            this.Email = email;
            this.Nom = nom;
            this.Tel = tel;
            this.Adresse = adresse;
            this.Logo = logo;
        }
        public string ToString()
        {
            return this.Nom + " | " + this.Logo + " | " + this.Adresse + " | " + this.Email + " | " + this.Tel;
        }
    }
}
