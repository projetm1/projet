﻿using ConsoleAppTest.modele;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjetFinalM1.modele
{
    public class JetonsHelper
    {
        private static string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        private static int taille = 15;
        private static Random random = new Random();
        public static string RandomString()
        {
            return new string(Enumerable.Repeat(chars, taille)
            .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string GenerateJeton(Jetons jetons, string key, string addToValue)
        {
            string jeton = RandomString() + addToValue;
            jetons.Add(key, jeton);
            return jeton;
        }
        public static Boolean Contains(Jetons jetons, string value)
        {
            return jetons.ContainsValue(value);
        }
        public static Boolean Contains(Jetons jetons, string key, string value)
        {
            string found;
            if (jetons.TryGetValue(key, out found) && found == value)
            {
                return true;
            }
            return false;
        }

        public static Boolean Authentifier(Utilisateur u, Jetons jetons)
        {
            if (!Contains(jetons, u.Email, u.Jeton))
                throw new Exception("Veuillez vous connectez avec un compte valide!");
            return true;
        }
        public static Boolean Authentifier(string jeton, Jetons jetons)
        {
            if (!Contains(jetons, jeton))
                throw new Exception("Veuillez vous connectez avec un compte valide!");
            return true;
        }
    }
}