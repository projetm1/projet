﻿using ConsoleAppTest.modele;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest.dao
{
    class FournisseurDAO
    {
        public static void save(Fournisseur f)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                save(f, conn);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        public static void save(Fournisseur f, SqlConnection conn)
        {
            string query = "INSERT INTO _FOURNISSEUR (nom,email,tel,adresse,logo) VALUES (@nom,@email,@tel,@adresse,@logo)";
            try
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@nom", f.Nom);
                cmd.Parameters.AddWithValue("@email", f.Email);
                cmd.Parameters.AddWithValue("@tel", f.Tel);
                cmd.Parameters.AddWithValue("@adresse", f.Adresse);
                cmd.Parameters.AddWithValue("@logo", f.Logo);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("impossible de sauvegarder :" + e.Message);
            }
        }

        public static void update(Fournisseur f)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                update(f, conn);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        public static void update(Fournisseur f, SqlConnection conn)
        {
            string query = "UPDATE _FOURNISSEUR SET nom=@nom, email=@email, tel=@tel, adresse=@adresse, logo=@logo WHERE id=@id";
            try
            {
                if (f.Id == 0)
                    throw new Exception("Mise à jour impossible, ID manquant");
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@id", f.Id);
                cmd.Parameters.AddWithValue("@nom", f.Nom);
                cmd.Parameters.AddWithValue("@email", f.Email);
                cmd.Parameters.AddWithValue("@tel", f.Tel);
                cmd.Parameters.AddWithValue("@adresse", f.Adresse);
                cmd.Parameters.AddWithValue("@logo", f.Logo);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Console.WriteLine("L'erreur suivante a été rencontrée :" + e.Message);
            }
        }

        public static int getIdByNameAndSaveIfNotExist(Fournisseur f, SqlConnection conn)
        {
            int fournisseurID = 0;
            string query = "SELECT TOP(1) * from _FOURNISSEUR where nom = @nom";
            SqlDataReader dataReader = null;
            try
            {
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@nom", f.Nom);
                    dataReader = cmd.ExecuteReader();
                    while (dataReader.Read())
                    {
                        fournisseurID = (int)dataReader["id"];
                        Console.WriteLine("Fournisseur identifié id = " + fournisseurID);
                    }
                    dataReader.Close();
                    if (fournisseurID == 0)
                    {
                        fournisseurID = saveAndGetId(f, conn);
                        Console.WriteLine("Sauvegarde fournisseur, id = " + fournisseurID);
                    }
                    return fournisseurID;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }

        public static int saveAndGetId(Fournisseur f, SqlConnection conn)
        {
            string queryInsert = "INSERT INTO _FOURNISSEUR (nom, email, tel, adresse, logo) output INSERTED.ID values (@nom,@email,@tel,@adresse,@logo)";
            try
            {
                SqlCommand cmdInsert = new SqlCommand(queryInsert, conn);
                cmdInsert.Parameters.AddWithValue("@nom", f.Nom);
                cmdInsert.Parameters.AddWithValue("@email", f.Email);
                cmdInsert.Parameters.AddWithValue("@tel", f.Tel);
                cmdInsert.Parameters.AddWithValue("@adresse", f.Adresse);
                cmdInsert.Parameters.AddWithValue("@logo", f.Logo);
                return (int) cmdInsert.ExecuteScalar();
            }
            catch (Exception e) { throw e; }
        }
        public static void findById(Fournisseur f)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                findById(f, conn);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (conn.State == System.Data.ConnectionState.Open)
                    conn.Close();
            }
        }
        public static void findById(Fournisseur f, SqlConnection conn)
        {
            string query = "select top(1) * from _FOURNISSEUR where id = @id";
            SqlDataReader dataReader = null;
            try
            {
                if (f.Id == 0)
                    throw new Exception("Recherche impossible, ID manquant");
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@id", f.Id);
                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    f.Nom = (string)dataReader["nom"];
                    f.Adresse = (string)dataReader["adresse"];
                    f.Tel = (string)dataReader["tel"];
                    f.Email = (string)dataReader["email"];
                    f.Logo = (string)dataReader["logo"];
                }

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }
    }
}
