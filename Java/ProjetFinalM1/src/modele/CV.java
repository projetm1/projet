/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

/**
 *
 * @author Rasolonirina
 */
public class CV extends BaseModele{
    private String nom;
    private String tel;
    private String adresse;
    private String email;
    private String poste;
    private String type;

    public CV() {
    }
    
    public CV(String nom, String tel, String address, String email, String post, String type) {
        this.nom = nom;
        this.tel = tel;
        this.adresse = address;
        this.email = email;
        this.poste = post;
        this.type = type;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String address) {
        this.adresse = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPoste() {
        return poste;
    }

    public void setPoste(String post) {
        this.poste = post;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    public String toString(){
        return "nom:"+nom+" address:"+adresse+" tel:"+tel+" email:"+email+" post:"+poste+" type:"+type;
    }
}
