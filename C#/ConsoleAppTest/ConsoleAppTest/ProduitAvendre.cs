﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest.modele
{
    class ProduitAvendre:ProduitEntre
    {
        private double pu;

        public double Pu
        {
            get { return pu; }
            set { pu = value; }
        }
        public ProduitAvendre(string designation, double pu):base(designation) {
            this.Pu = pu;
        }
    }
}
