

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.*;
import java.sql.SQLClientInfoException;
//import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
//import com.microsoft.sqlserver.jdbc.SQLServerException;


public class Connexion {
	
	private static Connection conn = null;
	
	public Connexion()throws ClassNotFoundException, SQLException{}
	
	public static Connection getConnection() throws ClassNotFoundException, SQLException
	{
		try
		{
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				conn = DriverManager.getConnection("jdbc:sqlserver://localhost:49163;databaseName=projetfinalm1;integratedSecurity=true");
		}
		catch(Exception e)
		{
				e.printStackTrace();
		}
		return conn;
	}
}

