/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

import java.io.PrintWriter;
import java.util.ArrayList;
import modele.DetailFacture;
import modele.Facture;

/**
 *
 * @author Kiady Asandratra
 */
public class ToFacture {

    String file;

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public ToFacture(String file) {
        this.file = file;
    }

    public Facture toFacture() {
        Facture res = new Facture();
        String txt = loadFile();
        // System.out.println(txt);
        String[] line = txt.split("\n");
        //  System.out.println(line.length);
        ArrayList<DetailFacture> body = new ArrayList<>();
        int i = 0, j = 0;
        res.setFournisseur(line[0]);
        for (i = 1; i < line.length; i++) {
            // System.out.println(i+"  "+line[i]);
            if (ToFacture.contient(line[i], "desigantion") || ToFacture.contient(line[i], "pu") || ToFacture.contient(line[i], "qtte") || ToFacture.contient(line[i], "quantite") || ToFacture.contient(line[i], "prix") || ToFacture.contient(line[i], "prix unitaire")) {
                //System.out.println("niditra");
                break;
            }
            if (ToFacture.contient(line[i], "date") && res.getDate() == null) {
                String[] temp = line[i].split(" ");
                res.setDate(temp[temp.length - 1]);
            } else if (ToFacture.contient(line[i], "reference") || ToFacture.contient(line[i], "numero") || ToFacture.contient(line[i], "id") && res.getReference() == null) {
                String[] temp = line[i].split(" ");
                res.setReference(temp[temp.length - 1]);

            } else {
                String txte = res.getRemarque() + line[i] + ";";
                res.setRemarque(txte);

            }

        }
        for (i = i + 1; i < line.length; i++) {

            if (ToFacture.contient(line[i], "tva") || ToFacture.contient(line[i], "net") || ToFacture.contient(line[i], "ht") || ToFacture.contient(line[i], "ttc")) {
                break;
            }
            String[] temp = line[i].split(" ");
//            for (int k = 0; k < temp.length; k++) {
//               
//            }
            if (temp.length > 2) {
                DetailFacture detailFacture = new DetailFacture();

                if (ToFacture.contient(temp[0])) {
                    detailFacture.setQuantite(temp[0]);
                    String s = "";
                    for (int k = 1; k < temp.length; k++) {
                        if (ToFacture.contient(temp[k])) {
                            break;
                        }
                        s += temp[k]+" ";

                    }
                    detailFacture.setProduit(s);
                } else {
                    String s = "", qtt = "";
                    int k = 0;
                    for (k = 0; k < temp.length; k++) {

                        if (ToFacture.contient(temp[k])) {
                            qtt = temp[k];
                            break;
                        }
                        s += temp[k]+" ";
                    }
                    detailFacture.setProduit(s);
                    // System.out.println(k + " " + temp[k]+" "+temp.length);

                    detailFacture.setQuantite(qtt);
                }

                detailFacture.setPu(temp[temp.length-2]);
                detailFacture.setTotal(temp[temp.length-1]);

                body.add(detailFacture);
            }
        }

        for (i = i; i < line.length; i++) {
            //System.out.println(line[i]);
            if (!ToFacture.contient(line[i])) {
                break;
            } else if (ToFacture.contient(line[i], "tva")) {
                String[] temp = line[i].split("%");
               // System.out.println(temp[0]);
                String[] tva = temp[0].split(" ");
                res.setTVA(tva[tva.length-1]);
            }
            if (ToFacture.contient(line[i], "total")) {
                String[] temp = line[i].split(" ");
                res.setTotal(temp[temp.length - 1]);
            }
        }
        res.setArrayList(body);
        return res;
    }

    public static boolean contient(String init, String another) {
//        System.out.println(init+"   "+another);
//        System.out.println("metier.ToFacture.contient() "+init.toLowerCase().split(another).length);
        return init.toLowerCase().contains(another.toLowerCase());
    }

    public static boolean contient(String init) {
        boolean b = false;
        for (Integer i = 0; i < 10; i++) {
            if (init.toLowerCase().contains(i.toString().toLowerCase())) {
                b = true;
                break;
            }
        }
        return b;
    }

    public String loadFile() {

        String outpout = "out";
        String tesseract_install = "D:\\Programs\\Tesseract-OCR\\tesseract";
        String[] command
                = {
                    "cmd",};
        Process p;
        try {
            p = Runtime.getRuntime().exec(command);
            new Thread(new SyncPipe(p.getErrorStream(), System.err)).start();
            new Thread(new SyncPipe(p.getInputStream(), System.out)).start();
            PrintWriter stdin = new PrintWriter(p.getOutputStream());
            stdin.println("\"" + tesseract_install + "\" \"" + file + "\" \"" + outpout + "\" ");

            stdin.close();
            p.waitFor();

            return Read.read(outpout + ".txt");

        } catch (Exception e) {
            return null;
        }
    }
}
