﻿using ConsoleAppTest.dao;
using ConsoleAppTest.modele;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest
{
    class Program
    {
        static void Main(string[] args)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                Utilisateur u = new Utilisateur();
                u.Nom = "Constraint";
                u.Prenom = "Test";
                u.Email = "admin";
                u.Mdp = "test";
                UtilisateurDAO.save(u);
                Console.WriteLine(u.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                conn.Close();
                Console.ReadLine();
            }
        }
    }
}
