﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest
{
    class BaseModele
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public BaseModele() { }
        public BaseModele(int id) { this.Id = id; }
    }
}
