﻿using Serveur.dao;
using Serveur.modele;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Serveur
{
    /// <summary>
    /// Description résumée de FactureService
    /// </summary>
    [WebService(Namespace = "http://projetFinalM1/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Pour autoriser l'appel de ce service Web depuis un script à l'aide d'ASP.NET AJAX, supprimez les marques de commentaire de la ligne suivante. 
    // [System.Web.Script.Services.ScriptService]
    public class FactureService : System.Web.Services.WebService
    {

        [WebMethod(Description="Sauvegarde facture")]
        public Facture saveFacture(bool?suite, Facture f)
        {
            try
            {
                if (f.Details.Count == 0)
                    throw new Exception("La facture ne contient aucun produit.");
                FactureDao.save(suite, f);
                return f;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
