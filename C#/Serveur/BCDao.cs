﻿using Serveur.modele;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Serveur.dao
{
    public class BCDao
    {
        public static void save(BC bc)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                save(bc, conn);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        public static void save(BC bc, SqlConnection conn)
        {
            string query = "INSERT INTO ";
            if (bc.IsClient)
                query += "BDCCLIENT values (idUser, idClient,";
            else query += "BDCFOURNISSEUR values (idUser, idFournisseur,";
            query += "date,remarque) values ("+bc.IdUser+","+bc.IdClientFournisseur+","+bc.Date+",'"+bc.Remarque+"')";
            try
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message, null);
            }
        }
        
        public static int getLastId(bool bdcclient, SqlConnection conn)
        {
            string table = "BDCCLIENT";
            if (!bdcclient) table = "BDCFOURNISSEUR";
            string query = "SELECT TOP(1) id FROM "+ table +" ORDER BY 1 DESC";
            SqlDataReader dataReader = null;
            try
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    return (int)dataReader["id"];
                }
                dataReader.Close();
                return 0;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }
        public static void update(BC bc)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                update(bc, conn);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void update(BC bc, SqlConnection conn)
        {
            string query = "UPDATE ";
            if(bc.IsClient){
                query += " BDCCLIENT SET (date='" + bc.Date + "', idUser=" + bc.IdUser + ", remarque='" + bc.Remarque + "',idClient="+bc.IdClientFournisseur+ ");";
            }else{
                query += " BDCFOURNISSEUR SET (date='" + bc.Date + "', idUser=" + bc.IdUser + ", remarque='" + bc.Remarque + "',idFournisseur=" + bc.IdClientFournisseur + ");";
            }
            try
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static BC findById(BC bc)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                return findById(bc, conn);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        public static BC findById(BC bc, SqlConnection conn)
        {
            string table = "BDCCLIENT";
            if (!bc.IsClient) table = "BDCFOURNISSEUR";
            string query = "SELECT * FROM "+table+" where id=" + bc.Id;
            BC rep = new BC();
            SqlDataReader dataReader = null;
            try
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    rep.IdUser = (int)dataReader["idUser"];
                    rep.Remarque = (string)dataReader["remarque"];
                    rep.Date = (string)dataReader["date"];
                    if (bc.IsClient)
                    {
                        rep.Id = (int)dataReader["IDBDCCLIENT"];
                        rep.IdClientFournisseur = (int)dataReader["IDCLIENT"];
                    }
                    else
                    {
                        rep.Id = (int)dataReader["IDBDCFOURNISSEUR"];
                        rep.IdClientFournisseur = (int)dataReader["IDFOURNISSEUR"];
                    }
                }
                return rep;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }
        public static void delete(BC bc)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                delete(bc, conn);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        public static void delete(BC bc, SqlConnection conn)
        {
            string query = "DELETE FROM ";
            if (bc.IsClient) query += "BDCCLIENT ";
            else query += "BDCFOURNISSEUR ";
            query += "WHERE id=" + bc.Id;
            try
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /*public static List<Facture> findAll(SqlConnection conn){
            ArrayList<Facture> list = new ArrayList<Facture>();
            string strRequete = "SELECT * FROM Facture";
            try{
                SqlCommand oCommand = new SqlCommand(strRequete,oConnection);
                oConnection.Open();
                SqlDataReader oReader = oCommand.ExecuteReader();
                do
                {
                }
            }
            catch(Exception e){
                Console.WriteLine("L'erreur suivante a été rencontrée :" + e.Message);
            }
            return list;
        }*/
    }
}