﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Serveur.modele
{
    public class Facture:BaseModele
    {
        private string date;
        private List<FactureDetail> details;
        private int fournisseurId;
        private string remarque;
        private int montant;
        private string reference;
        private int tva;
        private int idUtilisateur;

        public int IdUtilisateur
        {
            get { return idUtilisateur; }
            set { idUtilisateur = value; }
        }

        public string Date
        {
            get { return date; }
            set { date = value; }
        }

        public string Remarque
        {
            get { return remarque; }
            set { remarque = value; }
        }

        public int FournisseurId
        {
            get { return fournisseurId; }
            set { fournisseurId = value; }
        }

        public int Montant
        {
            get { return montant; }
            set { montant = value; }
        }

        public string Reference
        {
            get { return reference; }
            set { reference = value; }
        }

        public int Tva
        {
            get { return tva; }
            set { tva = value; }
        }

        public List<FactureDetail> Details
        {
            get { return details; }
            set { details = value; }
        }
        public Facture(int id) : base(id) { }
        public Facture() { this.Details = new List<FactureDetail>(); }
        public Facture(string date, string remarque, int fournisseurID, int montant, string reference, int TVA, List<FactureDetail> details)
        {
            this.Date = date;
            this.Remarque = remarque;
            this.FournisseurId = fournisseurID;
            this.Montant = montant;
            this.Reference = reference;
            this.Tva = TVA;
            this.Details = details;
        }

        public void addDetail(FactureDetail fd)
        {
            this.Details.Add(fd);
        }
    }
}