<%-- 
    Document   : listDocument
    Created on : 8 juin 2018, 09:54:03
    Author     : Tsito
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>ProjetM1FinalM1</title>
        <link rel="stylesheet" href="src/css/w3.css">
            <script src="assets/js/jquery.js"></script>
            <link rel="stylesheet" href="assets/css/bootstrap.min.css">
            <link rel="stylesheet" href="assets/css/styles.css">
            <link rel="stylesheet" href="assets/css/stylemenu.css">
            <script src="assets/js/jquery-1.8.3.min.js"></script>
            <script src="assets/js/bootstrap.min.js"></script>
            <script src="assets/js/script.js"></script>
            <script src="assets/js/indexmenu.js"></script>

            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
            <script src="assets/js/angular.min.js"></script>
            <script src="assets/js/angular-animate.min.js"></script>
            <link rel="stylesheet/less" type="text/css" href="assets/less/style.less" />
            <style>
                table {
                    border-collapse: collapse;
                    width: 100%;
                }

                th, td {
                    text-align: left;
                    padding: 8px;
                }

                tr:nth-child(even){background-color: #f2f2f2}

                th {
                    background-color: #3399ff;
                    color: white;
                }
            </style>
    </head>
    <body>
            <div class="row">
                <!-- Sidebar -->
                <div class="col-md-2">
                    <nav class="menu">
                        <header class="avatar">
                            <h3 style="color : white;"></h3>
                            <p><a href="deconnecter">Se deconnecter</a></p>
                        </header><hr>
                    </nav>
                </div>
                <div class="col-md-10">
                    <!-- Page Content -->
                    <div class="form-style-12" >
                        <h1>Liste des documents</h1>                   
                        <div class="inner-wrap">
                            <form action="recherche" class="form-inline">
                                <div class="form-group">
                                    <label>Reference</label>  
                                    <input style="width : 100%;" id="" type="text" value="" name="nom" placeholder=""/>
                                </div>   
                                <div class="form-group">
                                    <label>Catégorie document</label>
                                    <select name="idCategorie" style="width : 100%;">
                                            <option value="">Tous</option>
                                            <option value="">Carte de visite</option>                                               
                                            <option value="">Facture</option>
                                            <option value="">Bon de commande</option>
                                    </select>
                                </div> 
                                <div class="form-group"><br><br>
                                    <input  type="submit" class="btn btn-default" value="Rechercher"/>
                                </div>    
                            </form>
                        </div>
                        <div class="row">
                            <div class="col-md-10">
                            </div>
                            <div class="col-md-2"> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                                <table>                        
                                                    <tr>
                                                        <th>Reference</th>
                                                        <th>Type</th>
                                                        <th>Date</th>
                                                    </tr>
                                                    <tr class="animate-repeat" ng-repeat="x in names| filter:test" >
                                                        <td><a href=""></a></td>
                                                        <td><a href=""></a></td>
                                                        <td><a href=""></a></td>  
                                                     </tr>
                                                </table>
                                         </div>
                                        <div class="row">
                                                <div class="col-lg-12">
                                                    <ul class="pagination">
                                                        <li class="nomClass"><a href="">1</a></li>
                                                        <li class="nomClass"><a href="">2</a></li>
                                                        <li class="nomClass"><a href="">3</a></li>
                                                        <li class="nomClass"><a href="">4</a></li>
                                                    </ul>  
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
    </body>
</html>
