﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.IO;
using Serveur.modele;

namespace Serveur.dao
{
    public class FactureDao
    {
        public static void save(bool? suite, Facture f)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                save(suite, f, conn);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        
        public static void save(bool? suite, Facture f, SqlConnection conn)
        {
            int factureId = 0;
            int idByRef = 0;
            string queryFacture = "";

            string queryDetail = "INSERT INTO FACTURE_DETAIL (id,pu,quantite,factureId) values ";
            try
            {
                idByRef = findByRef(f.Reference, conn);
                if (suite == false || suite == null)
                {
                    if (idByRef > 0)
                        throw new Exception("Facture déjà enregistrée!",null);
                    queryFacture = "INSERT INTO Facture (date, idFournisseur, idUtilisateur, reference, montant, tva, remarque) values ('" + f.Date + "','" + f.FournisseurId +","+ f.IdUtilisateur +"','" + f.Reference + "'," + f.Montant + "," + f.Tva + ",'" + f.Remarque + "')";
                    SqlCommand cmd = new SqlCommand(queryFacture, conn);
                    cmd.ExecuteNonQuery();
                    factureId = getLastId(conn);
                }
                else
                {
                    factureId = idByRef;
                }
                for (int i = 0; i < f.Details.Count; i++)
                {
                    Produit p = new Produit();
                    p.Designation = f.Details[i].Designation;
                    p.Pu = f.Details[i].Pu;
                    ProduitDao.findByNameAndSaveIfNotExist(p, conn);
                    
                    queryDetail = queryDetail + "(" +
                        p.Pu + "," +
                        f.Details[i].Quantite + "," +
                        factureId + ")";
                    if (i < f.Details.Count - 1)
                        queryDetail = queryDetail + ",";
                }
                SqlCommand cmd2 = new SqlCommand(queryDetail, conn);
                cmd2.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception( e.Message, null );
            }
        }
        
        

        public static int findByRef(string reference, SqlConnection conn)
        {
            string query = "SELECT TOP(1) id FROM Facture where reference = '" + reference + "'";
            SqlDataReader dataReader = null;
            try
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    return (int)dataReader["id"];

                }
                return 0;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }
        public static int getLastId(SqlConnection conn)
        {
            string query = "SELECT TOP(1) id FROM Facture ORDER BY 1 DESC";
            SqlDataReader dataReader = null;
            try
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    return (int)dataReader["id"];
                }
                dataReader.Close();
                return 0;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }
        public static void update(Facture f)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                update(f, conn);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void update(Facture f, SqlConnection conn)
        {
            string query = "UPDATE Facture SET (date='"+f.Date+"', fournisseur='"+f.FournisseurId+"', total="+f.Montant+", tva="+f.Tva+", remarque='"+f.Remarque+"')";
            try{
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void findById(Facture f)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                findById(f, conn);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        public static void findById(Facture f, SqlConnection conn){
            string query = "SELECT * FROM Facture where id=" + f.Id;
            SqlDataReader dataReader = null;
			try{
				SqlCommand cmd = new SqlCommand(query, conn);
                dataReader = cmd.ExecuteReader();
				while (dataReader.Read())
                {
                    f.Reference = (string)dataReader["reference"];
                    f.IdUtilisateur = (int)dataReader["idUtilisateur"];
                    f.Remarque = (string)dataReader["remarque"];
                    f.Montant = (int)dataReader["montant"];
                    f.Tva = (int)dataReader["tva"];
                    f.Date = (string)dataReader["date"];
                }
			}
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
		}
        public static void delete(Facture f)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                delete(f, conn);
            }
            catch (Exception e){
                throw new Exception(e.Message);
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        public static void delete(Facture f, SqlConnection conn)
        {
            string query = "DELETE FROM Facture WHERE id=" + f.Id + "";
            try
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /*public static List<Facture> findAll(SqlConnection conn){
            ArrayList<Facture> list = new ArrayList<Facture>();
            string strRequete = "SELECT * FROM Facture";
            try{
                SqlCommand oCommand = new SqlCommand(strRequete,oConnection);
                oConnection.Open();
                SqlDataReader oReader = oCommand.ExecuteReader();
                do
                {
                }
            }
            catch(Exception e){
                Console.WriteLine("L'erreur suivante a été rencontrée :" + e.Message);
            }
            return list;
        }*/
    }
}