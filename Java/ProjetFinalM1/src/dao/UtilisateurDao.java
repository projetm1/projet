/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import modele.Utilisateur;



/**
 *
 * @author tsito
 */
public class UtilisateurDao {
    public static void save(Utilisateur u,Connection conn)throws ClassNotFoundException, SQLException{
       Statement statement = null;
        try {
            conn.setAutoCommit(false);
            String query = "INSERT INTO Utilisateur (nom,prenom,email,mdp) VALUES ('" + u.getNom() + "','" + u.getPrenom() + "','" + u.getEmail() + "','" + u.getMdp() + "')";            
            statement.executeUpdate(query);
            conn.commit();
        } catch (Exception e) {
            conn.rollback();
            throw e;
        } finally {
            if (statement != null) {
                statement.close();
            }
        }
    }
    public static void findAll(Connection conn)throws ClassNotFoundException, SQLException{
        Statement statement = null;
        try {
            
        }
        catch (Exception e) {
            conn.rollback();
            throw e;
        } finally {
            if (statement != null) {
                statement.close();
            }
        }
    }
    public static Utilisateur findById(Utilisateur u,Connection conn)throws ClassNotFoundException, SQLException{
            Utilisateur rep = new Utilisateur();
            Statement statement = null;
            try
            {
                String query = "SELECT * FROM Utilisateur where id = " + u.getId() + "";
                ResultSet rs = statement.executeQuery(query); 
                while (rs.next())
                {
                    u.setId(rs.getInt("id"));
                    u.setNom(rs.getString("nom"));
                    u.setPrenom(rs.getString("prenom"));
                    u.setEmail(rs.getString("email"));
                    u.setMdp(rs.getString("mdp"));
                }
                return rep;

            }
            catch (Exception e)
            {
                throw e;
            }finally{
                if (statement != null)
                    statement.close();
            }
    }
    public static Utilisateur authentification(Utilisateur u,Connection conn)throws ClassNotFoundException, SQLException{
            Utilisateur rep = new Utilisateur();
            Statement statement = null;
            try
            {
                String query = "select * from utilisateur where email='" + u.getEmail() + "' and mdp='" + u.getMdp() + "'";
                ResultSet rs = statement.executeQuery(query); 
                while (rs.next())
                {
                    u.setId(rs.getInt("id"));
                    u.setNom(rs.getString("nom"));
                    u.setPrenom(rs.getString("prenom"));
                    u.setEmail(rs.getString("email"));
                    u.setMdp(rs.getString("mdp"));
                }
                return rep;

            }
            catch (Exception e)
            {
                throw e;
            }finally{
                if (statement != null)
                    statement.close();
            }
    } 
    public static void delete(Utilisateur u,Connection conn)throws ClassNotFoundException, SQLException{        
        Statement statement = null;
        try {
            conn.setAutoCommit(false);
            String query = "DELETE FROM Utilisateur where id = " + u.getId() + "";
            statement.executeQuery(query);
            conn.commit();
        }
        catch (Exception e) {
            conn.rollback();
            throw e;
        } finally {
            if (statement != null) {
                statement.close();
            }
        }
    }
    public static void update(Utilisateur u,Connection conn)throws ClassNotFoundException, SQLException{
        Statement statement = null;
        try {
            conn.setAutoCommit(false);
            String query = "UPDATE Utilisateur SET (nom='" + u.getNom() + "', prenom='" + u.getPrenom() + "', email='" + u.getEmail() + "', mdp='" + u.getMdp() + "' WHERE id=" + u.getId() + ")";
            statement.executeQuery(query);
            conn.commit();
        }
        catch (Exception e) {
            conn.rollback();
            throw e;
        } finally {
            if (statement != null) {
                statement.close();
            }
        }
    }
}
