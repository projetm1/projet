﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Serveur.modele
{
    public class Produit:BaseModele
    {
        private string designation;
        private double pu;

        public double Pu
        {
            get { return pu; }
            set { pu = value; }
        }
        
        public string Designation
        {
            get { return designation; }
            set { designation = value; }
        }
        private bool entre;

        public bool Entre
        {
            get { return entre; }
            set { entre = value; }
        }
        public Produit()
        {
            this.Entre = false;
        }
    }
}