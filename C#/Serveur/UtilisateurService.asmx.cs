﻿using Serveur.dao;
using Serveur.modele;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Serveur
{
    /// <summary>
    /// Description résumée de UtilisateurService
    /// </summary>
    [WebService(Namespace = "http://projetFinalM1/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Pour autoriser l'appel de ce service Web depuis un script à l'aide d'ASP.NET AJAX, supprimez les marques de commentaire de la ligne suivante. 
    // [System.Web.Script.Services.ScriptService]
    public class UtilisateurService : System.Web.Services.WebService
    {
        [WebMethod(Description="Pour authentifier l'utilisateur")]
        public Utilisateur Authentifier(string email, string mdp)
        {
            try
            {
                Utilisateur u = UtilisateurDao.authentifier(email, mdp);
                return u;
            }
            catch (Exception ex)
            {
                throw new Exception("Erreur d'authentification avec l' email:("+email+") et le mdp:("+mdp+") erreur:"+ex.Message);
            }
        }

        [WebMethod(Description = "Pour s'inscrire")]
        public Utilisateur Inscription(Utilisateur u)
        {
            try
            {
                UtilisateurDao.save(u);
                return u;
            }
            catch (Exception ex)
            {
                throw new Exception("Erreur lors de l'inscription erreur:" + ex.Message);
            }
        }
    }
}
