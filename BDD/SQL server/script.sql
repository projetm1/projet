/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2008                    */
/* Created on:     5/18/2018 6:07:24 AM                         */
/*==============================================================*/


if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('BDCCLIENT') and o.name = 'FK_BDCCLIEN_CLIENTBDC_CLIENT')
alter table BDCCLIENT
   drop constraint FK_BDCCLIEN_CLIENTBDC_CLIENT
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('BDCCLIENT') and o.name = 'FK_BDCCLIEN_USERBDCC_USER')
alter table BDCCLIENT
   drop constraint FK_BDCCLIEN_USERBDCC_USER
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('BDCFOURNISSEUR') and o.name = 'FK_BDCFOURN_FOURNISSE_FOURNISS')
alter table BDCFOURNISSEUR
   drop constraint FK_BDCFOURN_FOURNISSE_FOURNISS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('BDCFOURNISSEUR') and o.name = 'FK_BDCFOURN_USERBDCF_USER')
alter table BDCFOURNISSEUR
   drop constraint FK_BDCFOURN_USERBDCF_USER
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('DETAILBDCC') and o.name = 'FK_DETAILBD_DETAILBDC_PRODUITF')
alter table DETAILBDCC
   drop constraint FK_DETAILBD_DETAILBDC_PRODUITF
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('DETAILBDCC') and o.name = 'FK_DETAILBD_DETAILBDC_BDCCLIEN')
alter table DETAILBDCC
   drop constraint FK_DETAILBD_DETAILBDC_BDCCLIEN
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('DETAILBDCF') and o.name = 'FK_DETAILBD_DETAILBDC_MATIEREP')
alter table DETAILBDCF
   drop constraint FK_DETAILBD_DETAILBDC_MATIEREP
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('DETAILBDCF') and o.name = 'FK_DETAILBD_DETAILBDC_BDCFOURN')
alter table DETAILBDCF
   drop constraint FK_DETAILBD_DETAILBDC_BDCFOURN
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('DETAILFACTURE') and o.name = 'FK_DETAILFA_DETAILFAC_MATIEREP')
alter table DETAILFACTURE
   drop constraint FK_DETAILFA_DETAILFAC_MATIEREP
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('DETAILFACTURE') and o.name = 'FK_DETAILFA_DETAILFAC_FACTURE')
alter table DETAILFACTURE
   drop constraint FK_DETAILFA_DETAILFAC_FACTURE
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('FACTURE') and o.name = 'FK_FACTURE_USERFACTU_USER')
alter table FACTURE
   drop constraint FK_FACTURE_USERFACTU_USER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('BDCCLIENT')
            and   name  = 'USERBDCC_FK'
            and   indid > 0
            and   indid < 255)
   drop index BDCCLIENT.USERBDCC_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('BDCCLIENT')
            and   name  = 'CLIENTBDC_FK'
            and   indid > 0
            and   indid < 255)
   drop index BDCCLIENT.CLIENTBDC_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('BDCCLIENT')
            and   type = 'U')
   drop table BDCCLIENT
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('BDCFOURNISSEUR')
            and   name  = 'FOURNISSEURBDC_FK'
            and   indid > 0
            and   indid < 255)
   drop index BDCFOURNISSEUR.FOURNISSEURBDC_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('BDCFOURNISSEUR')
            and   name  = 'USERBDCF_FK'
            and   indid > 0
            and   indid < 255)
   drop index BDCFOURNISSEUR.USERBDCF_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('BDCFOURNISSEUR')
            and   type = 'U')
   drop table BDCFOURNISSEUR
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CARTEDEVISITE')
            and   type = 'U')
   drop table CARTEDEVISITE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CLIENT')
            and   type = 'U')
   drop table CLIENT
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DETAILBDCC')
            and   name  = 'DETAILBDCC2_FK'
            and   indid > 0
            and   indid < 255)
   drop index DETAILBDCC.DETAILBDCC2_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('DETAILBDCC')
            and   type = 'U')
   drop table DETAILBDCC
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DETAILBDCF')
            and   name  = 'DETAILBDCF_FK'
            and   indid > 0
            and   indid < 255)
   drop index DETAILBDCF.DETAILBDCF_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('DETAILBDCF')
            and   type = 'U')
   drop table DETAILBDCF
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DETAILFACTURE')
            and   name  = 'DETAILFACTURE_FK'
            and   indid > 0
            and   indid < 255)
   drop index DETAILFACTURE.DETAILFACTURE_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('DETAILFACTURE')
            and   type = 'U')
   drop table DETAILFACTURE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('FACTURE')
            and   name  = 'USERFACTURE_FK'
            and   indid > 0
            and   indid < 255)
   drop index FACTURE.USERFACTURE_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('FACTURE')
            and   type = 'U')
   drop table FACTURE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('FOURNISSEUR')
            and   type = 'U')
   drop table FOURNISSEUR
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MATIEREPREMIERE')
            and   type = 'U')
   drop table MATIEREPREMIERE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PRODUITFINI')
            and   type = 'U')
   drop table PRODUITFINI
go

if exists (select 1
            from  sysobjects
           where  id = object_id('"USER"')
            and   type = 'U')
   drop table "USER"
go

/*==============================================================*/
/* Table: BDCCLIENT                        bon de commande client(BBDCC)                     */
/*==============================================================*/
create table BDCCLIENT (
   IDBDCCLIENT          numeric              identity,
   IDUSER               int                  not null,
   IDCLIENT             int                  not null,
   DATE                 datetime             null,
   REMARQUE             varchar(76)          null,
   constraint PK_BDCCLIENT primary key nonclustered (IDBDCCLIENT)
)
go

/*==============================================================*/
/* Index: CLIENTBDC_FK                                          */
/*==============================================================*/
create index CLIENTBDC_FK on BDCCLIENT (
IDCLIENT ASC
)
go

/*==============================================================*/
/* Index: USERBDCC_FK                                           */
/*==============================================================*/
create index USERBDCC_FK on BDCCLIENT (
IDUSER ASC
)
go

/*==============================================================*/
/* Table: BDCFOURNISSEUR                                        */
/*==============================================================*/
create table BDCFOURNISSEUR (
   IDBDCFOURNISSEUR     numeric              identity,
   IDUSER               int                  not null,
   IDFOURNISSEUR        int                  not null,
   DATE                 datetime             null,
   REMARQUE             varchar(76)          null,
   constraint PK_BDCFOURNISSEUR primary key nonclustered (IDBDCFOURNISSEUR)
)
go

/*==============================================================*/
/* Index: USERBDCF_FK                                           */
/*==============================================================*/
create index USERBDCF_FK on BDCFOURNISSEUR (
IDUSER ASC
)
go

/*==============================================================*/
/* Index: FOURNISSEURBDC_FK                                     */
/*==============================================================*/
create index FOURNISSEURBDC_FK on BDCFOURNISSEUR (
IDFOURNISSEUR ASC
)
go

/*==============================================================*/
/* Table: CARTEDEVISITE                                         */
/*==============================================================*/
create table CARTEDEVISITE (
   IDCARTEDEVISITE      numeric              identity,
   NOM                  varchar(65)          null,
   TELEPHONE            varchar(76)          null,
   ADRESSE              varchar(76)          null,
   EMAIL                varchar(76)          null,
   POSTE                varchar(76)          null,
   TYPE                 varchar(76)          null,
   constraint PK_CARTEDEVISITE primary key nonclustered (IDCARTEDEVISITE)
)
go

/*==============================================================*/
/* Table: CLIENT                                                */
/*==============================================================*/
create table CLIENT (
   IDCLIENT             numeric              identity,
   NOM                  varchar(65)          null,
   ADRESSE              varchar(76)          null,
   CONTACT              varchar(76)          null,
   REMARQUE             varchar(76)          null,
   constraint PK_CLIENT primary key nonclustered (IDCLIENT)
)
go

/*==============================================================*/
/* Table: DETAILBDCC                detail bon de commande client(BBDCC)    */
/*==============================================================*/
create table DETAILBDCC (
   IDBDCCLIENT          int                  not null,
   IDPRODUIT            int                  not null,
   ID                   numeric              identity,
   PRIX                 int                  null,
   QUANTITE             int                  null,
   constraint PK_DETAILBDCC primary key nonclustered (ID)
)
go

/*==============================================================*/
/* Index: DETAILBDCC2_FK                                        */
/*==============================================================*/
create index DETAILBDCC2_FK on DETAILBDCC (
IDBDCCLIENT ASC
)
go

/*==============================================================*/
/* Table: DETAILBDCF            detail bon de commande FOURNISSEUR(BBDCF)          */
/*==============================================================*/
create table DETAILBDCF (
   IDBDCFOURNISSEUR     int                  not null,
   IDMATIEREPREMIERE    int                  not null,
   IDDETAILBDCF         numeric              identity,
   PRIX                 int                  null,
   QUANTITE             int                  null,
   constraint PK_DETAILBDCF primary key nonclustered (IDDETAILBDCF)
)
go

/*==============================================================*/
/* Index: DETAILBDCF_FK                                         */
/*==============================================================*/
create index DETAILBDCF_FK on DETAILBDCF (
IDMATIEREPREMIERE ASC
)
go

/*==============================================================*/
/* Table: DETAILFACTURE                                         */
/*==============================================================*/
create table DETAILFACTURE (
   ID                   int                  not null,
   IDMATIEREPREMIERE    int                  not null,
   IDDETAILFACUTRE      numeric              identity,
   PRIX                 int                  null,
   QUANTITE             int                  null,
   constraint PK_DETAILFACTURE primary key nonclustered (IDDETAILFACUTRE)
)
go

/*==============================================================*/
/* Index: DETAILFACTURE_FK                                      */
/*==============================================================*/
create index DETAILFACTURE_FK on DETAILFACTURE (
IDMATIEREPREMIERE ASC
)
go

/*==============================================================*/
/* Table: FACTURE                                               */
/*==============================================================*/
create table FACTURE (
   ID                   numeric              identity,
   IDUSER               int                  not null,
   DATE                 datetime             null,
   REMARQUE             varchar(76)          null,
   constraint PK_FACTURE primary key nonclustered (ID)
)
go

/*==============================================================*/
/* Index: USERFACTURE_FK                                        */
/*==============================================================*/
create index USERFACTURE_FK on FACTURE (
IDUSER ASC
)
go

/*==============================================================*/
/* Table: FOURNISSEUR                                           */
/*==============================================================*/
create table FOURNISSEUR (
   IDFOURNISSEUR        numeric              identity,
   NOM                  varchar(65)          null,
   ADRESSE              varchar(76)          null,
   LOGO                 varchar(76)          null,
   CONTACT              varchar(76)          null,
   constraint PK_FOURNISSEUR primary key nonclustered (IDFOURNISSEUR)
)
go

/*==============================================================*/
/* Table: MATIEREPREMIERE                                       */
/*==============================================================*/
create table MATIEREPREMIERE (
   IDMATIEREPREMIERE    numeric              identity,
   NOM                  varchar(65)          null,
   constraint PK_MATIEREPREMIERE primary key nonclustered (IDMATIEREPREMIERE)
)
go

/*==============================================================*/
/* Table: PRODUITFINI                                           */
/*==============================================================*/
create table PRODUITFINI (
   IDPRODUIT            numeric              identity,
   NOM                  varchar(65)          null,
   constraint PK_PRODUITFINI primary key nonclustered (IDPRODUIT)
)
go

/*==============================================================*/
/* Table: "USER"                                                */
/*==============================================================*/
create table "USER" (
   IDUSER               numeric              identity,
   NOM                  varchar(65)          null,
   PREMON               varchar(65)          null,
   MAIL                 varchar(65)          null,
   MDP                  varchar(65)          null,
   constraint PK_USER primary key nonclustered (IDUSER)
)
go

alter table BDCCLIENT
   add constraint FK_BDCCLIEN_CLIENTBDC_CLIENT foreign key (IDCLIENT)
      references CLIENT (IDCLIENT)
go

alter table BDCCLIENT
   add constraint FK_BDCCLIEN_USERBDCC_USER foreign key (IDUSER)
      references "USER" (IDUSER)
go

alter table BDCFOURNISSEUR
   add constraint FK_BDCFOURN_FOURNISSE_FOURNISS foreign key (IDFOURNISSEUR)
      references FOURNISSEUR (IDFOURNISSEUR)
go

alter table BDCFOURNISSEUR
   add constraint FK_BDCFOURN_USERBDCF_USER foreign key (IDUSER)
      references "USER" (IDUSER)
go

alter table DETAILBDCC
   add constraint FK_DETAILBD_DETAILBDC_PRODUITF foreign key (IDPRODUIT)
      references PRODUITFINI (IDPRODUIT)
go

alter table DETAILBDCC
   add constraint FK_DETAILBD_DETAILBDC_BDCCLIEN foreign key (IDBDCCLIENT)
      references BDCCLIENT (IDBDCCLIENT)
go

alter table DETAILBDCF
   add constraint FK_DETAILBD_DETAILBDC_MATIEREP foreign key (IDMATIEREPREMIERE)
      references MATIEREPREMIERE (IDMATIEREPREMIERE)
go

alter table DETAILBDCF
   add constraint FK_DETAILBD_DETAILBDC_BDCFOURN foreign key (IDBDCFOURNISSEUR)
      references BDCFOURNISSEUR (IDBDCFOURNISSEUR)
go

alter table DETAILFACTURE
   add constraint FK_DETAILFA_DETAILFAC_MATIEREP foreign key (IDMATIEREPREMIERE)
      references MATIEREPREMIERE (IDMATIEREPREMIERE)
go

alter table DETAILFACTURE
   add constraint FK_DETAILFA_DETAILFAC_FACTURE foreign key (ID)
      references FACTURE (ID)
go

alter table FACTURE
   add constraint FK_FACTURE_USERFACTU_USER foreign key (IDUSER)
      references "USER" (IDUSER)
go

