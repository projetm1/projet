﻿using Serveur.modele;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Serveur.dao
{
    public class CVDao
    {
        public static void save(CV cv)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                save(cv, conn);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        public static void save(CV cv,SqlConnection conn)
        {
            string query = "INSERT INTO CARTEDEVISITE (nom,email,tel,adresse,societe,poste,type) values ";
            try
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message, null);
            }
        }
        
        public static int getLastId(SqlConnection conn)
        {
            string query = "SELECT TOP(1) id FROM CARTEDEVISITE ORDER BY 1 DESC";
            SqlDataReader dataReader = null;
            try
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    return (int)dataReader["id"];
                }
                dataReader.Close();
                return 0;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }
        public static void update(CV cv)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                update(cv, conn);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void update(CV cv, SqlConnection conn)
        {
            string query = "UPDATE Facture SET (nom='" + cv.Nom + "', email='" + cv.Email + "', tel='" + cv.Tel + "', adresse='" + cv.Adresse + "', type='"+ cv.Type + "', societe='"+ cv.Societe+"', poste='"+ cv.Poste+"')";
            try
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static Facture findById(CV cv)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                return findById(cv, conn);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        public static Facture findById(CV cv, SqlConnection conn)
        {
            string query = "SELECT * FROM CARTEVISITE where id=" + cv.Id;
            Facture rep = new Facture();
            SqlDataReader dataReader = null;
            try
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    rep.Id = (int)dataReader["id"];
                    rep.Reference = (string)dataReader["reference"];
                    rep.Remarque = (string)dataReader["remarque"];
                    rep.Montant = (int)dataReader["montant"];
                    rep.Tva = (int)dataReader["tva"];
                    rep.Date = (string)dataReader["date"];
                }
                return rep;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }
        public static void delete(CV cv)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                delete(cv, conn);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        public static void delete(CV cv, SqlConnection conn)
        {
            string query = "DELETE FROM CARTEDEVISITE WHERE id=" + cv.Id + "";
            try
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /*public static List<Facture> findAll(SqlConnection conn){
            ArrayList<Facture> list = new ArrayList<Facture>();
            string strRequete = "SELECT * FROM Facture";
            try{
                SqlCommand oCommand = new SqlCommand(strRequete,oConnection);
                oConnection.Open();
                SqlDataReader oReader = oCommand.ExecuteReader();
                do
                {
                }
            }
            catch(Exception e){
                Console.WriteLine("L'erreur suivante a été rencontrée :" + e.Message);
            }
            return list;
        }*/
    }
}