﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest.modele
{
    class Fournisseur:Personne
    {
        private string logo;

        public string Logo
        {
            get { return logo; }
            set { logo = value; }
        }
        public Fournisseur() { }

        public Fournisseur(int id):base(id)
        {
        }

        public string ToString()
        {
            return this.Nom + " | " + this.Logo + " | " + this.Adresse + " | " + this.Email + " | " + this.Tel;
        }
    }
}
