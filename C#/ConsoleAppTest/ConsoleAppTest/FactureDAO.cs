﻿using ConsoleAppTest.modele;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest.dao
{
    class FactureDAO
    {
        public static void save(Facture f)
        {
            save(false, f);
        }
        public static void save(bool? suite, Facture f)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                save(suite, f, conn);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        public static void save(bool? suite, Facture f, SqlConnection conn)
        {
            int factureId = 0;
            int idByRef = 0;
            string queryFacture = "";
            try
            {
                if (f.Details.Count == 0)
                    throw new Exception("Le facture ne contient aucun produit");
                idByRef = getIdByRef(f.Reference, conn);
                int fournisseurID = FournisseurDAO.getIdByNameAndSaveIfNotExist(f.Fournisseur, conn);
                if (suite == false || suite == null)
                {
                    if (idByRef > 0)
                        throw new Exception("Facture déjà enregistrée!", null);
                    queryFacture = "INSERT INTO _Facture (daty, fournisseurID, utilisateurID, reference, avoir, montant, tva, vise, remarque) output INSERTED.ID values (@daty,@fournisseurID,@utilisateurID,@reference,@avoir,@montant,@tva,@vise,@remarque)";
                    SqlCommand cmd = new SqlCommand(queryFacture, conn);
                    cmd.Parameters.AddWithValue("@daty", f.Daty);
                    cmd.Parameters.AddWithValue("@fournisseurID", fournisseurID);
                    cmd.Parameters.AddWithValue("@utilisateurID", f.UtilisateurID);
                    cmd.Parameters.AddWithValue("@reference", f.Reference);
                    cmd.Parameters.AddWithValue("@avoir", f.Avoir);
                    cmd.Parameters.AddWithValue("@montant", f.Montant);
                    cmd.Parameters.AddWithValue("@tva", f.Tva);
                    cmd.Parameters.AddWithValue("@vise", f.Vise);
                    cmd.Parameters.AddWithValue("@remarque", f.Remarque);

                    factureId = (int)cmd.ExecuteScalar();
                }
                else
                {
                    factureId = idByRef;
                }
                for (int i = 0; i < f.Details.Count; i++)
                {
                    ProduitEntre p = new ProduitEntre();
                    FactureDetail fd = new FactureDetail();
                    p.Designation = f.Details[i].Designation;
                    fd.Designation = f.Details[i].Designation;
                    fd.FactureID = factureId;
                    fd.Quantite = f.Details[i].Quantite;

                    ProduitDAO.findByNameAndSaveIfNotExist(p, conn);
                    fd.ProduitID = p.Id;
                    FactureDAO.saveDetail(fd, conn);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message, null);
            }
        }

        

        public static int getIdByRef(string reference, SqlConnection conn)
        {
            string query = "SELECT TOP(1) id FROM _Facture where reference = @reference";
            SqlDataReader dataReader = null;
            try
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@reference", reference);
                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    return (int)dataReader["id"];
                }
                return 0;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }
        
        public static void update(Facture f)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                update(f, conn);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void update(Facture f, SqlConnection conn)
        {
            string query = "UPDATE _Facture SET reference=@reference, daty=@daty, fournisseurID=@fournisseurID,avoir=@avoir, montant=@montant, tva=@tva, vise=@vise, remarque=@remarque where id = @id";
            try
            {
                if (f.Id == 0)
                    throw new Exception("Mise à jour impossible, ID manquant");
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@id", f.Id);
                cmd.Parameters.AddWithValue("@reference", f.Reference);
                cmd.Parameters.AddWithValue("@daty", f.Daty);
                cmd.Parameters.AddWithValue("@fournisseurID", f.Fournisseur.Id);
                cmd.Parameters.AddWithValue("@avoir", f.Avoir);
                cmd.Parameters.AddWithValue("@montant", f.Montant);
                cmd.Parameters.AddWithValue("@tva", f.Tva);
                cmd.Parameters.AddWithValue("@vise", f.Vise);
                cmd.Parameters.AddWithValue("@remarque", f.Remarque);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        
        public static void findById(Facture f)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                findById(f, conn);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (conn.State == System.Data.ConnectionState.Open)
                    conn.Close();
            }
        }
        public static void findById(Facture f, SqlConnection conn)
        {
            string query = "SELECT top(1) * FROM _Facture where id=@id";
            SqlDataReader dataReader = null;
            try
            {
                if (f.Id == 0)
                    throw new Exception("Recherche impossible, ID manquant");
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@id", f.Id);
                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    f.Reference = (dataReader["reference"] == DBNull.Value)?String.Empty:(string)dataReader["reference"];
                    f.UtilisateurID = (int)dataReader["utilisateurID"];
                    f.Fournisseur.Id = (int)dataReader["fournisseurID"];
                    f.Remarque = (dataReader["remarque"] == DBNull.Value) ? String.Empty : (string)dataReader["remarque"];
                    f.Montant = Convert.ToDouble(dataReader["montant"]);
                    f.Tva = (int)dataReader["tva"];
                    f.Vise = (int)dataReader["vise"];
                    f.Daty = (string)dataReader["daty"].ToString();
                }
                dataReader.Close();
                FournisseurDAO.findById(f.Fournisseur, conn);
                f.Details = findDetailByFactureId(f.Id, conn);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }
        public static void delete(Facture f)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                delete(f, conn);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        public static void delete(Facture f, SqlConnection conn)
        {
            string query = "DELETE FROM _Facture WHERE id=@id";
            try
            {
                if (f.Id == 0)
                    throw new Exception("Suppression impossible, ID manquant");
                deleteDetailByFactureId(f.Id);
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@id", f.Id);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public static List<Facture> findAll()
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                return findAll(conn);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (conn.State == System.Data.ConnectionState.Open)
                    conn.Close();
            }
        }
        public static List<Facture> findAll(SqlConnection conn)
        {
            string query = "SELECT * from _FACTURE";
            List<Facture> res = new List<Facture>();
            SqlDataReader dataReader = null;
            try{
                SqlCommand cmd = new SqlCommand(query, conn);
                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    Facture f = new Facture((int)dataReader["id"]);
                    f.UtilisateurID = (int)dataReader["utilisateurID"];
                    f.Montant = Convert.ToDouble(dataReader["montant"]);
                    f.Tva = (int)dataReader["tva"];
                    f.Daty = (string)dataReader["daty"].ToString();
                    f.Avoir = Convert.ToDouble(dataReader["avoir"]);
                    f.Vise = (int) dataReader["vise"];
                    f.Reference = (dataReader["reference"] == DBNull.Value) ? String.Empty : (string)dataReader["reference"];
                    f.Remarque = (dataReader["remarque"] == DBNull.Value) ? String.Empty : (string)dataReader["remarque"];
                    f.Fournisseur.Id = (int)dataReader["fournisseurID"];
                    res.Add(f);
                }
                dataReader.Close();
                for (int i = 0; i < res.Count; i++)
                {
                    FournisseurDAO.findById(((Facture)res[i]).Fournisseur, conn);
                    ((Facture)res[i]).Details = findDetailByFactureId(((Facture)res[i]).Id, conn);
                }
                    return res;
            }catch(Exception e){
                throw e;
            }

        }
        /******************************************************
         *              F A C T U R E    D E T A I L
         * ***************************************************/

        public static void save(FactureDetail fd)
        {
            saveDetail(fd);
        }
        public static void save(FactureDetail fd, SqlConnection conn)
        {
            saveDetail(fd, conn);
        }
        public static void saveDetail(FactureDetail detail)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                saveDetail(detail, conn);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        public static void saveDetail(FactureDetail detail, SqlConnection conn)
        {
            string query = "INSERT INTO _FACTURE_DETAIL (factureID, produitID, designation, pu, quantite) values (@factureID, @produitID, @designation, @pu, @quantite)";
            try
            {
                if (detail.FactureID == 0)
                    throw new Exception("Sauvegarde du detail impossible, ID facture manquant");
                if (detail.ProduitID == 0)
                    throw new Exception("Sauvegarde du detail impossible, ID Produit manquant");
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@factureID", detail.FactureID);
                cmd.Parameters.AddWithValue("@produitID", detail.ProduitID);
                cmd.Parameters.AddWithValue("@designation", detail.Designation);
                cmd.Parameters.AddWithValue("@pu", detail.Pu);
                cmd.Parameters.AddWithValue("@quantite", detail.Quantite);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public static void update(FactureDetail fd)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                update(fd, conn);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void update(FactureDetail fd, SqlConnection conn)
        {
            string query = "UPDATE _FACTURE_DETAIL SET factureID=@factureID, produitID =@produitID, designation=@designation, pu=@pu, quantite=@quantite,remarque=@remarque where id = @id";
            try
            {
                if (fd.Id == 0)
                    throw new Exception("Mise à jour impossible, ID manquant");
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@id", fd.Id);
                cmd.Parameters.AddWithValue("@factureID", fd.FactureID);
                cmd.Parameters.AddWithValue("@produitID", fd.ProduitID);
                cmd.Parameters.AddWithValue("@pu", fd.Pu);
                cmd.Parameters.AddWithValue("@quantite", fd.Quantite);
                cmd.Parameters.AddWithValue("@designation", fd.Designation);
                cmd.Parameters.AddWithValue("@remarque", fd.Remarque);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public static void findById(FactureDetail fd)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                findById(fd, conn);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (conn.State == System.Data.ConnectionState.Open)
                    conn.Close();
            }
        }
        public static void findById(FactureDetail fd, SqlConnection conn)
        {
            string query = "SELECT top(1) * FROM _Facture_Detail where id=@id";
            SqlDataReader dataReader = null;
            try
            {
                if (fd.Id == 0)
                    throw new Exception("Recherche impossible, ID manquant");
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@id", fd.Id);
                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    fd.Designation = (dataReader["designation"] == DBNull.Value) ? String.Empty : (string)dataReader["designation"];
                    fd.ProduitID = (int)dataReader["produitID"];
                    fd.FactureID = (int)dataReader["factureID"];
                    fd.Remarque = (dataReader["remarque"] == DBNull.Value) ? String.Empty : (string)dataReader["remarque"];
                    fd.Pu = Convert.ToDouble(dataReader["pu"]);
                    fd.Quantite = (int)dataReader["quantite"];
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }

        public static List<FactureDetail> findDetailByFactureId(int factureID)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                return findDetailByFactureId(factureID, conn);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (conn.State == System.Data.ConnectionState.Open)
                    conn.Close();
            }
        }
        public static List<FactureDetail> findDetailByFactureId(int factureID, SqlConnection conn)
        {
            string query = "SELECT * FROM _Facture_Detail where factureID=@factureID";
            SqlDataReader dataReader = null;
            try
            {
                if (factureID < 1)
                    throw new Exception("Recherche impossible, factureID doit être >= 1");
                List<FactureDetail> res = new List<FactureDetail>();
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@factureID", factureID);
                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    res.Add(new FactureDetail(
                        (int)dataReader["id"],
                        factureID,
                        (int)dataReader["produitID"],
                        (dataReader["designation"] == DBNull.Value) ? String.Empty : (string)dataReader["designation"],
                        Convert.ToDouble(dataReader["pu"]),
                        (int)dataReader["quantite"],
                        (dataReader["remarque"] == DBNull.Value) ? String.Empty : (string)dataReader["remarque"]
                    ));
                }
                return res;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }

        public static void delete(FactureDetail fd)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                delete(fd, conn);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (conn.State == System.Data.ConnectionState.Open)
                    conn.Close();
            }
        }
        public static void delete(FactureDetail fd, SqlConnection conn)
        {
            try
            {
                if (fd.Id == 0)
                    throw new Exception("Suppression impossible, ID manquant");
                string query = "DELETE FROM _FACTURE_DETAIL where id=@id";
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@id", fd.Id);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public static List<FactureDetail> findAllDetail()
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                return findAllDetail(conn);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (conn.State == System.Data.ConnectionState.Open)
                    conn.Close();
            }
        }
        public static List<FactureDetail> findAllDetail(SqlConnection conn)
        {
            string query = "SELECT * FROM _Facture_Detail";
            SqlDataReader dataReader = null;
            try
            {
                List<FactureDetail> res = new List<FactureDetail>();
                SqlCommand cmd = new SqlCommand(query, conn);
                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    res.Add(new FactureDetail(
                        (int)dataReader["id"],
                        (int)dataReader["factureID"],
                        (int)dataReader["produitID"],
                        (dataReader["designation"] == DBNull.Value) ? String.Empty : (string)dataReader["designation"],
                        Convert.ToDouble(dataReader["pu"]),
                        (int)dataReader["quantite"],
                        (dataReader["remarque"] == DBNull.Value) ? String.Empty : (string)dataReader["remarque"]
                    ));
                }
                return res;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }



        public static void deleteDetailByFactureId(int factureID)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnect.getConnexion();
                deleteDetailByFactureId(factureID, conn);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if (conn.State == System.Data.ConnectionState.Open)
                    conn.Close();
            }
        }
        public static void deleteDetailByFactureId(int factureID, SqlConnection conn)
        {
            try
            {
                if (factureID < 1)
                    throw new Exception("Suppression impossible, factureID doit être >= 1");
                string query = "DELETE FROM _FACTURE_DETAIL where factureID=@factureID";
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@factureID", factureID);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        
    }
}
